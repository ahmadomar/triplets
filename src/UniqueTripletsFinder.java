import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class UniqueTripletsFinder {
    public static List<List<Integer>> findTriplets(int[] nums, int target) {
        List<List<Integer>> triplets = new ArrayList<>();

        // Sorting the array to simplify the process
        Arrays.sort(nums);

        int n = nums.length;
        for (int i = 0; i < n - 2; i++) {
            // Avoid duplicates in the first element of the triplet
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            int left = i + 1;
            int right = n - 1;

            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];

                if (sum == target) {
                    triplets.add(Arrays.asList(nums[i], nums[left], nums[right]));

                    // Avoid duplicates in the second and third elements of the triplet
                    while (left < right && nums[left] == nums[left + 1]) {
                        left++;
                    }
                    while (left < right && nums[right] == nums[right - 1]) {
                        right--;
                    }

                    left++;
                    right--;
                } else if (sum < target) {
                    left++;
                } else {
                    right--;
                }
            }
        }

        return triplets;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = getValidIntegerInput(scanner);

        int[] nums = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            nums[i] = getValidIntegerInput(scanner);
        }

        System.out.print("Enter the target sum: ");
        int target = getValidIntegerInput(scanner);

        List<List<Integer>> triplets = findTriplets(nums, target);

        if (triplets.isEmpty()) {
            System.out.println("No triplets found.");
        } else {
            System.out.println("Triplets found:");
            for (List<Integer> triplet : triplets) {
                System.out.println(triplet);
            }
        }
    }

    private static int getValidIntegerInput(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            System.out.println("Invalid input. Please enter an integer.");
            scanner.next(); // Clear the invalid input from the scanner
        }
        return scanner.nextInt();
    }
}